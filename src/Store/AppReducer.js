import ActionTypes from "./ActionType";

export const initState = {
  configs: null,
  isMenuOpen: false,
};

export const reducer = (state = initState, action) => {
  let newState = state;
  switch (action.type) {
    case ActionTypes.SET_CONFIG:
      newState = {
        ...newState,
        configs: action.configs,
      };

      return newState;
    case ActionTypes.MENU_TOGGLE:
      newState = {
        ...newState,
        isMenuOpen: !newState.isMenuOpen,
      };
      return newState;

    default:
      return newState;
  }
};
