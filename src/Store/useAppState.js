import { useReducer, useContext } from "react";

import AppContext from "./AppContext";
import { reducer, initState } from "./AppReducer";

export const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export const useAppState = () => {
  return useContext(AppContext);
};
