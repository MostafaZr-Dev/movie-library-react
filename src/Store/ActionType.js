const Types = {
  SET_CONFIG: "SET_CONFIG",
  MENU_TOGGLE: "MENU_TOGGLE",
};

export default Types;
