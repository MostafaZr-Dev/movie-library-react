import axios from "axios";

const AccessToken = process.env.REACT_APP_ACCESS_TOKEN;

class HttpService {
  constructor() {
    this.client = axios.create({
      baseURL: "https://api.themoviedb.org/3",
      headers: {
        authorization: `Bearer ${AccessToken}`,
        "Content-Type": "application/json",
      },
    });
  }

  get(url, config = null) {
    return this.client.get(url, config);
  }
  post(url, params, config = null) {
    return this.client.post(url, params, config);
  }

  delete(url, params, config = null) {
    return this.client.delete(url, params, config);
  }
}

export default new HttpService();
