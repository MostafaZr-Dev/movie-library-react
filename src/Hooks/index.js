export { default as useGetMovies } from "./useGetMovies";
export { default as useGetMovie } from "./useGetMovie";
export { default as useGetMovieCast } from "./useGetMovieCast";
export { default as useGetQuery } from "./useGetQuery";
export { default as useGetGenres } from "./useGetGenres";
export { default as useGetRequest } from "./useGetRequest";
export { default as useGetConfigs } from "./useGetConfigs";
