import { useEffect, useState } from "react";

import HttpService from "Services/Http";

function useGetMovie(id) {
  const [isLoading, setIsLoading] = useState(false);
  const [casts, setCasts] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    function getMovie() {
      setIsLoading(true);
      HttpService.get(`/movie/${id}/credits`)
        .then((res) => {
          console.log(res.data);
          setCasts(res.data);
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setError(err);
          setIsLoading(false);
        });
    }

    getMovie();
  }, [id]);

  return { casts, error, isLoading };
}

export default useGetMovie;
