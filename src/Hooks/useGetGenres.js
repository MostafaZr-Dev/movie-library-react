import { useEffect, useState } from "react";

import HttpService from "Services/Http";

function useGetGenres() {
  const [isLoading, setIsLoading] = useState(false);
  const [genres, setGenres] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    function getRequest() {
      setIsLoading(true);
      HttpService.get("/genre/movie/list")
        .then((res) => {
          console.log(res.data);
          setGenres(res.data.genres);
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setError(err);
          setIsLoading(false);
        });
    }

    getRequest();
  }, []);

  return { genres, error, isLoading };
}

export default useGetGenres;
