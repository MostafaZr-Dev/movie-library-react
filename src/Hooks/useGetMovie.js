import { useEffect, useState } from "react";

import HttpService from "Services/Http";

function useGetMovie(id) {
  const [isLoading, setIsLoading] = useState(false);
  const [movie, setMovie] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    function getMovie() {
      setIsLoading(true);
      HttpService.get(`/movie/${id}?append_to_response=videos`)
        .then((res) => {
          console.log(res.data);
          setMovie(res.data);
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setError(err);
          setIsLoading(false);
        });
    }

    getMovie();
  }, [id]);

  return { movie, error, isLoading };
}

export default useGetMovie;
