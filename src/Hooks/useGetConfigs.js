import { useEffect, useState } from "react";

import HttpService from "Services/Http";
import { useAppState } from "Store/useAppState";
import ActionType from "Store/ActionType";

function useGetConfigs() {
  const [error, setError] = useState(null);

  const { dispatch } = useAppState();

  useEffect(() => {
    function getConfigs() {
      HttpService.get(`/configuration`)
        .then((res) => {
          console.log("getConfigs: ", res.data);
          dispatch({ type: ActionType.SET_CONFIG, configs: res.data });
        })
        .catch((err) => {
          console.log("getConfigs: ", err);
          setError(err);
        });
    }

    getConfigs();
  }, []);

  return { error };
}

export default useGetConfigs;
