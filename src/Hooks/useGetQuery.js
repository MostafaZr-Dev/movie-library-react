import { useLocation } from "react-router-dom";
import queryString from "query-string";

function useGetQuery() {
  const location = useLocation();
  const search = location.search;

  if (!search || search === "") {
    return null;
  }

  const parsedQuery = queryString.parse(search);
  return parsedQuery;
}

export default useGetQuery;
