import { useCallback, useEffect, useState } from "react";
import QueryString from "query-string";

import HttpService from "Services/Http";
import errorHandler from "Services/ErrorHandler";

function useGetMovies(url = null, params = null) {
  const [isLoading, setIsLoading] = useState(false);
  const [movies, setMovies] = useState([]);
  const [error, setError] = useState(null);

  let currentUrl = url;

  if (params) {
    const queryString = QueryString.stringify(params);
    currentUrl = `${currentUrl}?${queryString}`;
  }

  const getMovies = useCallback(() => {
    setIsLoading(true);
    HttpService.get(currentUrl)
      .then((res) => {
        console.log(res.data);
        setMovies(res.data.results);
        setIsLoading(false);
      })
      .catch((err) => {
        errorHandler(err);
        setError(err);
        setIsLoading(false);
      });
  }, [currentUrl]);

  useEffect(() => {
    if (url) {
      getMovies();
    }
  }, [currentUrl, url]);

  return { movies, error, isLoading };
}

export default useGetMovies;
