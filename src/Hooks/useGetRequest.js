import { useEffect, useState } from "react";

import HttpService from "Services/Http";

function useGetRequest(url) {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    function getRequest() {
      setIsLoading(true);
      HttpService.get(url)
        .then((res) => {
          console.log(res.data);
          setData(res.data.results);
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err);
          setError(err);
          setIsLoading(false);
        });
    }

    getRequest();
  }, [url]);

  return { data, error, isLoading };
}

export default useGetRequest;
