import { lazy } from "react";

const Home = lazy(() => import("Components/Routes/Home"));
const Popular = lazy(() => import("Components/Routes/Popular"));
const TopRated = lazy(() => import("Components/Routes/TopRated"));
const Upcoming = lazy(() => import("Components/Routes/Upcoming"));
const Genres = lazy(() => import("Components/Routes/Genres"));
const Single = lazy(() => import("Components/Routes/Single"));
const Search = lazy(() => import("Components/Routes/Search"));

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/popular",
    component: Popular,
  },
  {
    path: "/toprated",
    component: TopRated,
  },
  {
    path: "/upcoming",
    component: Upcoming,
  },
  {
    path: "/genres/:name",
    component: Genres,
  },
  {
    path: "/movie/:id",
    component: Single,
  },
  {
    path: "/search",
    component: Search,
  },
];

export default routes;
