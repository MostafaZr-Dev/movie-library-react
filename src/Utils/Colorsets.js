export const darkTheme = [
  "--bg-primary: #12132e",
  "--bg-secondary: #151634",
  "--txt-primary: #fff",
  "--txt-secondary: #c4c4c4",
  "--txt-active: #f30000",
  "--txt-inactive: #fff",
  "--shadow-color: #151736f7",
  "--icon-color: #fff",
  "--icon-inactive: #686868",
];
export const lightTheme = [
  "--bg-primary: #fff",
  "--bg-secondary: #eff3f4",
  "--txt-primary: #333",
  "--txt-secondary: #c4c4c4",
  "--txt-active: #f30000",
  "--txt-inactive: #c4c4c4",
  "--shadow-color: #9e9e9e42",
  "--icon-color: #0000008a",
  "--icon-inactive: #c4c4c4",
];
