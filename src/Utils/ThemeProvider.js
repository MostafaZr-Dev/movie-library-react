import { useCallback, useLayoutEffect, useState } from "react";

import ThemeContext from "./ThemeContext";
import { lightTheme, darkTheme } from "./Colorsets";

const applyTheme = (theme) => {
  const root = document.getElementsByTagName("html")[0];
  root.style.cssText = theme.join(";");
};

export const ThemeProvider = ({ children }) => {
  const [dark, setDark] = useState(false);

  const toggle = useCallback(() => {
    setDark(!dark);
    localStorage.setItem("darkTheme", !dark);
  }, [dark]);

  useLayoutEffect(() => {
    const lastTheme = localStorage.getItem("darkTheme");

    if (lastTheme === "true") {
      setDark(true);
      applyTheme(darkTheme);
    }

    if (!lastTheme || lastTheme === "false") {
      setDark(false);
      applyTheme(lightTheme);
    }
    
  }, [dark]);

  return (
    <ThemeContext.Provider
      value={{
        dark,
        toggle,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
};
