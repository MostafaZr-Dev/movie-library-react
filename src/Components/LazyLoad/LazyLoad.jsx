import LazyLoad from "react-lazyload";

function LazyLoadCustom({
  scrollContainer,
  children,
  placeholder,
  overflow,
  style,
  ...rest
}) {
  return (
    <LazyLoad
      width={200}
      height="100%"
      overflow={overflow}
      placeholder={placeholder}
      style={{ height: "100%", ...style }}
      scrollContainer={scrollContainer}
      {...rest}
    >
      {children}
    </LazyLoad>
  );
}

export default LazyLoadCustom;
