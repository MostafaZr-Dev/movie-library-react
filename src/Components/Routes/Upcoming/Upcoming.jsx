import "./Upcoming.css";
import { useCallback } from "react";
import { useHistory } from "react-router-dom";

import MovieWrapper from "Components/MovieWrapper";
import MovieItem from "Components/MovieItem";
import Rating from "Components/Rating";
import Pagination from "Components/Pagination";
import { AppLoader } from "Components/Loader";
import { useGetMovies, useGetQuery } from "Hooks";

function Upcoming() {
  const history = useHistory();

  const queryString = useGetQuery();
  const page = queryString?.page ? parseInt(queryString.page, 10) : 1;

  const { movies, error, isLoading } = useGetMovies("/movie/upcoming", {
    page,
  });

  const onNextPageClick = useCallback(() => {
    history.push(`/toprated?page=${page + 1}`);
  }, [page]);

  const onPrevPageClick = useCallback(() => {
    history.push(`/toprated?page=${page - 1}`);
  }, [page]);

  if (isLoading) {
    return <AppLoader className="route__loader" />;
  }

  return (
    <MovieWrapper
      headerTitle="Upcoming"
      headerSubTitle="Movies"
      header={
        <Pagination
          page={page}
          onNextClick={onNextPageClick}
          onPrevClick={onPrevPageClick}
        />
      }
      footer={
        <Pagination
          page={page}
          onNextClick={onNextPageClick}
          onPrevClick={onPrevPageClick}
        />
      }
    >
      {movies.map((movie) => (
        <MovieItem
          key={movie.id}
          to={`/movie/${movie.id}`}
          primary={movie.original_title}
          secondary={<Rating value={movie.vote_average / 2} />}
          icon={null}
          imageSrc={`${process.env.REACT_APP_IMAGE_URI}/w342${movie.poster_path}`}
          lazyScrollContainer=".scroll__wrapper"
          lazyScroll={true}
          isLazy={true}
        />
      ))}
    </MovieWrapper>
  );
}

export default Upcoming;
