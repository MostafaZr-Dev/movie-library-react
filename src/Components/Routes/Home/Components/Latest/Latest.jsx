import "./Latest.css";
import { Link } from "react-router-dom";

import ImageSlider from "Components/ImageSlider";
import Search from "Components/Search";
import ToggleTheme from "Components/ToggleTheme";
import { useGetMovies } from "Hooks";
import { AppLoader } from "Components/Loader";

function Latest() {
  const { movies, error, isLoading } = useGetMovies("/tv/popular");

  const popularTVImages = movies.slice(0, 5).map((item) => ({
    id: item.id,
    name: item.name,
    source: item.backdrop_path,
  }));

  const renderLatestImage = popularTVImages.map((image, index) => (
    <div className="slide__item" key={image.id}>
      <Link to={`/popular/tv/${image.id}`}>
        <img
          src={`https://image.tmdb.org/t/p/original/${image.source}`}
          alt={`${image.name}-img`}
        />
        <span className="item__name">{image.name}</span>
      </Link>
    </div>
  ));

  if (isLoading) {
    return <AppLoader className="route__loader" />;
  }

  return (
    <div className="home__latest">
      <div className="latest__header">
        <h3 className="header__title">Popular TV Shows</h3>
        <div className="header__icons">
          <ToggleTheme />
          <Search />
        </div>
      </div>
      <div className="latest__body">
        <div className="body__img">
          <ImageSlider images={renderLatestImage} />
        </div>
      </div>
    </div>
  );
}

export default Latest;
