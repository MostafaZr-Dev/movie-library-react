import { ClickAwayListener, IconButton, TextField } from "@material-ui/core";

import { MotionVariant } from "Components/Motion";

const searchVariants = {
  open: {},
  closed: {},
};

const txtFieldVariants = {
  open: {
    opacity: 1,
    width: "auto",
  },
  closed: {
    opacity: 0,
    width: 0,
  },
};

function Search({
  isOpen,
  onKeyPress,
  onSearchIconClick,
}) {
  return (
    <MotionVariant
      tag="div"
      variants={searchVariants}
      animate={isOpen ? "open" : "closed"}
      initial="closed"
      className="search__wrapper"
    >
      <MotionVariant tag="div" variants={txtFieldVariants}>
        <TextField
          id="standard-secondary"
          placeholder="search for a movie..."
          onKeyPress={onKeyPress}
        />
      </MotionVariant>
      <IconButton className="icon__search" onClick={onSearchIconClick}>
        <Search />
      </IconButton>
    </MotionVariant>
  );
}

export default Search;
