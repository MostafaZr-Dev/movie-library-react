import "./TopRated.css";

import MovieSlider from "Components/MovieSlider";
import MovieItem from "Components/MovieItem";
import Rating from "Components/Rating";
import { AppLoader } from "Components/Loader";
import { useGetMovies } from "Hooks";

function TopRated() {
  const { movies, error, isLoading } = useGetMovies("/movie/top_rated");

  if (isLoading) {
    return (
      <AppLoader title="TopRated Loading" className="route__loader mt-15" />
    );
  }
  return (
    <MovieSlider headerTitle="Top Rated" linkTo="/toprated">
      {movies.map((movie) => (
        <MovieItem
          key={movie.id}
          to={`/movie/${movie.id}`}
          primary={movie.title}
          secondary={<Rating value={parseInt(movie.vote_average / 2)} />}
          imageSrc={`${process.env.REACT_APP_IMAGE_URI}/w342${movie.poster_path}`}
          lazyScrollContainer=".slick-list"
          isLazy={false}
        />
      ))}
    </MovieSlider>
  );
}

export default TopRated;
