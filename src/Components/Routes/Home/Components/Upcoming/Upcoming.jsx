import MovieSlider from "Components/MovieSlider";
import MovieItem from "Components/MovieItem";
import { AppLoader } from "Components/Loader";
import Rating from "Components/Rating";
import { useGetMovies } from "Hooks";

function Upcoming() {
  const { movies, error, isLoading } = useGetMovies("/movie/upcoming");

  if (isLoading) {
    return (
      <AppLoader title="Upcoming Loading" className="route__loader mt-15" />
    );
  }

  return (
    <MovieSlider headerTitle="Upcoming" linkTo="/upcoming">
      {movies.map((movie) => (
        <MovieItem
          key={movie.id}
          to={`/movie/${movie.id}`}
          primary={movie.title}
          secondary={<Rating value={parseInt(movie.vote_average / 2)} />}
          imageSrc={`${process.env.REACT_APP_IMAGE_URI}/w342${movie.poster_path}`}
          isLazy={false}
          lazyScrollContainer=".main-slider .slick-list"
        />
      ))}
    </MovieSlider>
  );
}

export default Upcoming;
