import "./Home.css";

import Scrollbar from "Components/Scrollbar";
import Latest from "./Components/Latest";
import Popular from "./Components/Popular";
import TopRated from "./Components/TopRated";
import Upcoming from "./Components/Upcoming";

function Home() {
  return (
    <div className="home">
      <Scrollbar>
        <div className="home__wrapper">
          <Latest />
          <Popular />
          <TopRated />
          <Upcoming />
        </div>
      </Scrollbar>
    </div>
  );
}

export default Home;
