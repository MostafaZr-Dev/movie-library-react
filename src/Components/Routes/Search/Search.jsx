import "./Search.css";
import { useCallback, useState, useEffect, useLayoutEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";

import MovieWrapper from "Components/MovieWrapper";
import MovieItem from "Components/MovieItem";
import Pagination from "Components/Pagination";
import Rating from "Components/Rating";
import { AppLoader } from "Components/Loader";
import { useGetQuery, useGetMovies } from "Hooks";

function Search() {
  const query = useGetQuery();
  const history = useHistory();

  const [searchQuery, setSearchQuery] = useState(query?.s ? query.s : null);

  const page = query?.page ? parseInt(query.page, 10) : 1;

  const { movies, error, isLoading } = useGetMovies(
    searchQuery ? "/search/movie" : null,
    {
      query: searchQuery,
      page,
    }
  );

  useLayoutEffect(() => {
    setSearchQuery(query.s);

    return () => {
      setSearchQuery(null);
    };
  }, [query]);

  const onNextPageClick = useCallback(() => {
    history.push({
      pathname: `/search`,
      search: `?s=${searchQuery}&page=${page + 1}`,
    });
  }, [searchQuery, history, page]);

  const onPrevPageClick = useCallback(() => {
    history.push({
      pathname: `/search`,
      search: `?s=${searchQuery}&page=${page - 1}`,
    });
  }, [searchQuery, history, page]);

  if (!query?.s) {
    return <Redirect to="/" />;
  }

  if (isLoading) {
    return <AppLoader className="route__loader"/>;
  }

  return (
    <MovieWrapper
      headerTitle={searchQuery}
      headerSubTitle="Search Results"
      header={
        <>
          {!isLoading && (
            <Pagination
              page={page}
              onNextClick={onNextPageClick}
              onPrevClick={onPrevPageClick}
            />
          )}
        </>
      }
      footer={
        <>
          {isLoading && <AppLoader className="genres__loader" />}
          {!isLoading && (
            <Pagination
              page={page}
              onNextClick={onNextPageClick}
              onPrevClick={onPrevPageClick}
            />
          )}
        </>
      }
    >
      {movies.map((movie) => (
        <MovieItem
          to={`/movie/${movie.id}`}
          key={movie.id}
          primary={movie.title}
          secondary={<Rating value={movie.vote_average / 2} />}
          icon={null}
          imageSrc={`${process.env.REACT_APP_IMAGE_URI}/w342${movie.poster_path}`}
          lazyScrollContainer=".scroll__wrapper"
          isLazy={true}
        />
      ))}
    </MovieWrapper>
  );
}

export default Search;
