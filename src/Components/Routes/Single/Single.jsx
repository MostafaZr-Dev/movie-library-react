import "./Single.css";
import { useParams } from "react-router-dom";

import SingleMovie from "Components/SingleMovie";
import { AppLoader } from "Components/Loader";
import { useGetMovie, useGetMovieCast } from "Hooks";

function Single() {
  const { id } = useParams();

  const { movie, error, isLoading } = useGetMovie(id);
  const {
    casts,
    error: castError,
    isLoading: isLoadingCasts,
  } = useGetMovieCast(id);

  if (isLoading || isLoadingCasts) {
    return <AppLoader className="route__loader"/>;
  }

  return <SingleMovie movie={movie} casts={casts} />;
}

export default Single;
