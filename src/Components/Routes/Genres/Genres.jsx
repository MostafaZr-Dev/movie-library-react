import "./Genres.css";
import { useCallback, useLayoutEffect, useState } from "react";
import { useParams, useLocation, useHistory } from "react-router-dom";

import MovieWrapper from "Components/MovieWrapper";
import MovieItem from "Components/MovieItem";
import Rating from "Components/Rating";
import Pagination from "Components/Pagination";
import { AppLoader } from "Components/Loader";
import GenresHeader from "./Components/GenresHeader";
import { useGetDiscoverMovies } from "./Hooks";
import { useGetQuery } from "Hooks";

function Genres() {
  const history = useHistory();

  const { name } = useParams();
  const location = useLocation();
  const { genreId } = location.state ? location.state : { genreId: null };

  const sortStorage = localStorage.getItem("sortBy");
  const [sortBy, setSortBy] = useState(
    sortStorage ? sortStorage : "popularity.desc"
  );

  const queryString = useGetQuery();
  const page = queryString?.page ? parseInt(queryString.page, 10) : 1;

  useLayoutEffect(() => {
    return () => {
      localStorage.clear("sortBy");
      setSortBy("popularity.desc");
    };
  }, [genreId]);

  const { movies, error, isLoading } = useGetDiscoverMovies(
    "/discover/movie",
    genreId,
    {
      page,
      sort_by: sortBy,
      with_genres: genreId,
    }
  );

  // const { movies, error, isLoading } = useGetMovies("/discover/movie", {
  //   page,
  //   sort_by: sortBy,
  //   with_genres: genreId,
  // });

  const onNextPageClick = useCallback(() => {
    history.push({
      pathname: `/genres/${name}`,
      search: `?page=${page + 1}`,
      state: { genreId },
    });
  }, [page, name, genreId]);

  const onPrevPageClick = useCallback(() => {
    history.push({
      pathname: `/genres/${name}`,
      search: `?page=${page - 1}`,
      state: { genreId },
    });
  }, [page, name, genreId]);

  const onSortChange = useCallback(
    (sortBy, order) => {
      history.push({
        pathname: `/genres/${name}`,
        search: ``,
        state: { genreId },
      });
      setSortBy(`${sortBy}.${order}`);
      localStorage.setItem("sortBy", `${sortBy}.${order}`);
    },
    [name, genreId, history]
  );

  return (
    <MovieWrapper
      headerTitle={name}
      headerSubTitle="Movies"
      header={
        <>
          <GenresHeader
            key={name}
            defaultSort={sortBy.split(".").shift()}
            defaultOrder={sortBy.split(".").pop()}
            onSortChange={onSortChange}
          />
          {!isLoading && (
            <Pagination
              page={page}
              onNextClick={onNextPageClick}
              onPrevClick={onPrevPageClick}
            />
          )}
        </>
      }
      footer={
        <>
          {isLoading && <AppLoader className="genres__loader" />}
          {!isLoading && (
            <Pagination
              page={page}
              onNextClick={onNextPageClick}
              onPrevClick={onPrevPageClick}
            />
          )}
        </>
      }
    >
      {!isLoading && (
        <>
          {movies.map((movie) => (
            <MovieItem
              to={`/movie/${movie.id}`}
              key={movie.id}
              primary={movie.title}
              secondary={<Rating value={movie.vote_average / 2} />}
              icon={null}
              imageSrc={`${process.env.REACT_APP_IMAGE_URI}/w342${movie.poster_path}`}
              lazyScrollContainer=".scroll__wrapper"
              isLazy={true}
            />
          ))}
        </>
      )}
    </MovieWrapper>
  );
}

export default Genres;
