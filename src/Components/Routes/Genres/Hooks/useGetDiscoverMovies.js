import { useCallback, useEffect, useState } from "react";
import QueryString from "query-string";

import HttpService from "Services/Http";

function useGetDiscoverMovies(url, genreId, params = null) {
  const [isLoading, setIsLoading] = useState(false);
  const [movies, setMovies] = useState([]);
  const [error, setError] = useState(null);

  let currentUrl = url;

  if (params) {
    const queryString = QueryString.stringify(params);
    currentUrl = `${currentUrl}?${queryString}`;
  }

  const getDiscoverMovies = useCallback(() => {
    setIsLoading(true);
    HttpService.get(currentUrl)
      .then((res) => {
        console.log(res.data);
        setMovies(res.data.results);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setError(err);
        setIsLoading(false);
      });
  }, [currentUrl]);

  useEffect(() => {
    getDiscoverMovies();
  }, [genreId, getDiscoverMovies]);

  return { movies, error, isLoading };
}

export default useGetDiscoverMovies;
