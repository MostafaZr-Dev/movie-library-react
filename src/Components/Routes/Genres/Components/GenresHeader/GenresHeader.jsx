import "./GenresHeader.css";
import { useCallback, useEffect, useState, useRef } from "react";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";

function GenresHeader({ onSortChange, defaultSort, defaultOrder }) {
  const [sortBy, setSortBy] = useState(
    defaultSort ? defaultSort : "popularity"
  );
  const [order, setOrder] = useState(defaultOrder ? defaultOrder : "desc");

  const didMountRef = useRef(false);
  useEffect(() => {
    if (didMountRef.current) {
      onSortChange(sortBy, order);
    } else {
      didMountRef.current = true;
    }
  }, [sortBy, order]);

  const handleSortChange = useCallback((e) => {
    setSortBy(e.target.value);
  }, []);

  const handleOrderChange = useCallback((e) => {
    setOrder(e.target.value);
  }, []);

  return (
    <div className="genres__header">
      <FormControl component="fieldset" className="form__wrapper">
        <FormLabel component="legend" className="header__title">
          SortBy
        </FormLabel>
        <RadioGroup
          aria-label="order"
          name="order"
          value={defaultOrder}
          onChange={handleOrderChange}
          className="radio__group"
        >
          <FormControlLabel
            className="radio__feild"
            value="asc"
            control={<Radio />}
            label="Old"
          />
          <FormControlLabel
            className="radio__feild"
            value="desc"
            control={<Radio />}
            label="New"
          />
        </RadioGroup>
        <RadioGroup
          aria-label="sortby"
          name="sortby"
          value={defaultSort}
          onChange={handleSortChange}
          className="radio__group"
        >
          <FormControlLabel
            value="popularity"
            control={<Radio />}
            label="Popularity"
            className="radio__feild"
          />
          <FormControlLabel
            value="release_date"
            control={<Radio />}
            label="Release Date"
            className="radio__feild"
          />
          <FormControlLabel
            value="vote_average"
            control={<Radio />}
            label="Vote Average"
            className="radio__feild"
          />
          <FormControlLabel
            value="vote_count"
            control={<Radio />}
            label="Vote Count"
            className="radio__feild"
          />
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export default GenresHeader;
