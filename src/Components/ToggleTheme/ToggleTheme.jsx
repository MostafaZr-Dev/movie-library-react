import { useContext } from "react";
import { IconButton } from "@material-ui/core";
import { Brightness3, WbSunny } from "@material-ui/icons";

import ThemeContext from "Utils/ThemeContext";

function ToggleTheme() {
  const { dark, toggle } = useContext(ThemeContext);

  return (
    <IconButton onClick={toggle} className="toogle__theme">
      {!dark && <WbSunny />}
      {dark && <Brightness3 style={{ color: "yellow" }} />}
    </IconButton>
  );
}

export default ToggleTheme;
