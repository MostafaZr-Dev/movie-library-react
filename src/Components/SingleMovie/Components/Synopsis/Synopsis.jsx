import "./Synopsis.css";

function Synopsis({ overview }) {
  return (
    <div className="synopsis">
      <h6 className="synopsis__header">SYNOPSIS</h6>
      <span className="synopsis__body">{overview}</span>
    </div>
  );
}

export default Synopsis;
