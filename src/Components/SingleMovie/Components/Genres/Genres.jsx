import "./Genres.css";
import { RadioButtonChecked } from "@material-ui/icons";
import { Link } from "react-router-dom";

import { MotionHover } from "Components/Motion";

function Genres({ items }) {
  return (
    <div className="genres">
      <h6 className="genres__header">The Genres</h6>
      <div className="genres__body">
        {items.map((item) => (
          <Link
            to={{
              pathname: `/genres/${item.name}`,
              state: {
                genreId: item.id,
              },
            }}
            key={item.id}
          >
            <MotionHover whileHover={{ scale: 1.1 }}>
              <div className="genres__item">
                <RadioButtonChecked className="genres__icon" />
                <span className="genres__txt">{item.name}</span>
              </div>
            </MotionHover>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Genres;
