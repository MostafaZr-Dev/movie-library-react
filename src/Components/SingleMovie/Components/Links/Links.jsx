import "./Links.css";
import { useCallback } from "react";
import { Avatar, Chip } from "@material-ui/core";
import { Link } from "@material-ui/icons";

import { MotionHover } from "Components/Motion";

function Links({ imdb, website }) {
  const handleLinkClick = useCallback((url) => {
    window.open(url, "_blank");
  }, []);
  return (
    <div className="links">
      <MotionHover whileHover={{ scale: 1.1 }}>
        <Chip
          onClick={(e) => {
            handleLinkClick(website);
          }}
          icon={<Link />}
          label="Website"
          className="link"
        />
      </MotionHover>
      <MotionHover whileHover={{ scale: 1.1 }}>
        <Chip
          onClick={(e) => {
            handleLinkClick(imdb);
          }}
          avatar={
            <Avatar
              alt="Natacha"
              src={`${process.env.PUBLIC_URL}/assets/img/imdb.png`}
            />
          }
          label="IMDB"
          className="link"
        />
      </MotionHover>
    </div>
  );
}

export default Links;
