import "./Cast.css";
import { useCallback, useRef } from "react";
import { IconButton } from "@material-ui/core";
import { ArrowLeft, ArrowRight, MoodBad } from "@material-ui/icons";

import Slider from "Components/Slider";
import Image from "Components/Image";

function Cast({ items }) {
  const sliderRef = useRef();

  const onPrevClick = useCallback(() => {
    sliderRef.current.slickPrev();
  }, [sliderRef]);

  const onNextClick = useCallback(() => {
    sliderRef.current.slickNext();
  }, [sliderRef]);

  return (
    <div className="cast">
      <h6 className="cast__header">The Cast</h6>
      <div className="cast__items">
        {items?.length > 0 && (
          <>
            <IconButton className="slider__arrowLeft" onClick={onPrevClick}>
              <ArrowLeft />
            </IconButton>
            <IconButton className="slider__arrowRight" onClick={onNextClick}>
              <ArrowRight />
            </IconButton>
          </>
        )}
        {items?.length > 0 && (
          <Slider
            slidesToShow={items?.length < 4 ? items.length : 5}
            slidesToScroll={3}
            beforeChange={() => {}}
            ref={sliderRef}
            className="cast__slider"
            variableWidth={true}
            infinite={true}
            autoplay={true}
            autoplaySpeed={4000}
            speed={2000}
          >
            {items.map((item) => (
              <div
                key={item.cast_id}
                className="cast__item"
                style={{ width: "70px" }}
              >
                <Image
                  src={`${process.env.REACT_APP_IMAGE_URI}/w185${item.profile_path}`}
                  alt="cast-image"
                />
              </div>
            ))}
          </Slider>
        )}
        {items?.length === 0 && (
          <div className="empty__cast">
            <MoodBad />
            <span className="empty__txt"> No Casts </span>
            <MoodBad />
          </div>
        )}
      </div>
    </div>
  );
}

export default Cast;
