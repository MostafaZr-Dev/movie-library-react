import { useCallback, useState } from "react";
import { IconButton } from "@material-ui/core";
import { PlayArrowOutlined } from "@material-ui/icons";
import { Skeleton } from "@material-ui/lab";
import "./SingleImage.css";

import Modal from "Components/Modal";
import LazyLoad from "Components/LazyLoad";

function SingleImage({ src, releaseDate, trailer }) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [year, month, day] = releaseDate ? releaseDate : [];
  const date = new Date(year, month, day);
  const monthName = date.toLocaleString("default", { month: "short" });

  const handleCloseModal = useCallback(() => {
    setIsModalOpen(false);
  }, []);

  const handleClickTrailerBtn = useCallback(() => {
    setIsModalOpen(true);
  }, []);

  return (
    <div className="single__img">
      <LazyLoad
        placeholder={<Skeleton variant="rect" width={450} height={600} />}
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <img src={src} alt="single-img" />
      </LazyLoad>
      <span className="single__date">
        <span className="date__day">{day}</span>
        <span className="date__yearMonth">
          {monthName} {year}
        </span>
      </span>
      {!trailer && null}
      {trailer && (
        <span className="single__trailer">
          <IconButton className="trailer__icon" onClick={handleClickTrailerBtn}>
            <PlayArrowOutlined />
          </IconButton>
        </span>
      )}
      <Modal
        content={
          <iframe
            width="100%"
            height="90%"
            title="video-tr"
            allowFullScreen={true}
            src={`https://www.youtube.com/embed/${trailer?.key}?autoplay=1&controls=1&theme=dark`}
            frameborder="0"
          ></iframe>
        }
        isOpen={isModalOpen}
        handleClose={handleCloseModal}
      />
    </div>
  );
}

export default SingleImage;
