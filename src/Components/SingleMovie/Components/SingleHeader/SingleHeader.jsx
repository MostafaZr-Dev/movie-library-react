import "./SingleHeader.css";
import Lang from "./lang.json";

import Rating from "Components/Rating";

function SingleHeader({ title, subTitle, rating, time, lang, year }) {
  const languages = lang.reduce((prev, current) => [...prev, current.name], []);
  return (
    <div className="single__header">
      <h1 className="header__movieTitle">{title}</h1>
      <span className="header__movieTag">{subTitle}</span>
      <span className="header__movieDetails">
        {languages.join("-")} / {time}m / {year}
      </span>
      <div className="header__movieRating">
        <Rating value={rating} /> {rating}
      </div>
    </div>
  );
}

export default SingleHeader;
