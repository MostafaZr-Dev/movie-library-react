import "./SingleMovie.css";
import { useCallback } from "react";
import { Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

import Scrollbar from "Components/Scrollbar";
import SingleImage from "./Components/SingleImage";
import SingleHeader from "./Components/SingleHeader";
import Cast from "./Components/Cast";
import Genres from "./Components/Genres";
import Synopsis from "./Components/Synopsis";
import Links from "./Components/Links";
import ToggleTheme from "Components/ToggleTheme";
import Search from "Components/Search";

function SingleMovie({ movie, casts }) {
  const history = useHistory();

  const onBackClick = useCallback(() => {
    history.goBack();
  }, []);

  return (
    <Scrollbar>
      <div className="single">
        <div className="single__header">
          <div style={{ flex: 1 }}>
            <Button
              variant="contained"
              color="primary"
              size="small"
              startIcon={<ArrowBack />}
              onClick={onBackClick}
            >
              Back
            </Button>
          </div>
          <ToggleTheme />
          <Search />
        </div>
        <div className="single__wrapper">
          <SingleImage
            src={`${process.env.REACT_APP_IMAGE_URI}/w780${movie?.poster_path}`}
            releaseDate={movie?.release_date?.split("-")}
            trailer={movie?.videos?.results
              ?.filter((video) => video.type === "Trailer")
              .shift()}
          />
          <div className="single__details">
            <SingleHeader
              title={movie?.title}
              subTitle={movie?.tagline}
              rating={+(movie?.vote_average / 2)?.toFixed(1)}
              time={movie?.runtime}
              lang={movie?.spoken_languages ? movie.spoken_languages : []}
              year={movie?.release_date?.split("-").shift()}
            />
            <Cast items={casts?.cast ? casts.cast : []} />
            <Genres items={movie?.genres ? movie?.genres : []} />
            <Synopsis overview={movie?.overview} />
            <Links
              website={movie?.homepage}
              imdb={`https://www.imdb.com/title/${movie?.imdb_id}`}
            />
          </div>
        </div>
      </div>
    </Scrollbar>
  );
}

export default SingleMovie;
