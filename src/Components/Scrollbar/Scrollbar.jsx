import React from "react";
import { Scrollbars } from "react-custom-scrollbars";
import "./Scrollbar.css";

function Scrollbar({ children, ...props }) {
  return (
    <Scrollbars className="scrollbar" {...props}>
      {children}
    </Scrollbars>
  );
}

export default Scrollbar;
