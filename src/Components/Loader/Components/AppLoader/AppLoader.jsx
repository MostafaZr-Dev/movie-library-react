import "./AppLoader.css";

function AppLoader({ className, title = "Loading" }) {
  return (
    <div className={`app__loader ${className ? className : ""}`}>
      <div className="loader__filmstrip"></div>
      <p className="loader__text">{title}</p>
    </div>
  );
}

export default AppLoader;
