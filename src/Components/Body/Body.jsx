import "./Body.css";
import { useCallback, Suspense } from "react";

import routes from "Router";
import { RouteTransition, AnimatedRoutes } from "Components/Motion";
import MenuToggle from "Components/MenuToggle";
import { useAppState } from "Store/useAppState";
import ActionTypes from "Store/ActionType";
import { AppLoader } from "Components/Loader";

function Body() {
  const { state, dispatch } = useAppState();

  const handleMenuToggle = useCallback(() => {
    dispatch({ type: ActionTypes.MENU_TOGGLE });
  }, []);

  const appRoutes = routes.map((route, index) => (
    <RouteTransition exact={true} key={index} path={route.path}>
      <route.component />
    </RouteTransition>
  ));

  return (
    <div className="body">
      <MenuToggle isOpen={state.isMenuOpen} onClick={handleMenuToggle} />
      <Suspense fallback={<AppLoader className="route__loader" />}>
        <AnimatedRoutes exitBeforeEnter initial={false}>
          {appRoutes}
        </AnimatedRoutes>
      </Suspense>
    </div>
  );
}

export default Body;
