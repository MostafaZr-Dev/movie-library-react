import "./MovieSlider.css";
import { useCallback, useRef, useState } from "react";
import { IconButton } from "@material-ui/core";
import {
  ChevronLeft,
  ChevronRight,
} from "@material-ui/icons";
import { Link } from "react-router-dom";

import Slider from "Components/Slider";

function MovieSlider({ headerTitle, children, linkTo }) {
  const [arrowDisabled, setArrowDisabled] = useState("prev");

  const sliderRef = useRef();

  const handlePrevSlider = useCallback(() => {
    sliderRef.current.slickPrev();
  }, [sliderRef]);

  const handleNextSlider = useCallback(() => {
    sliderRef.current.slickNext();
    console.log();
  }, [sliderRef]);

  const onBeforeChangeSlide = useCallback((oldIndex, newIndex) => {
    const slideCount = sliderRef.current.props.children.length;
    const slideToShow = sliderRef.current.innerSlider.props.slidesToShow;

    if (newIndex === 0) {
      setArrowDisabled("prev");
      return;
    }

    if (slideCount === newIndex + slideToShow) {
      setArrowDisabled("next");
      return;
    }

    setArrowDisabled("");
  }, []);

  return (
    <div className="movie__slider">
      <div className="slider__header">
        <h3 className="header__title">{headerTitle}</h3>
        <div className="header__sliderArrow">
          <Link to={linkTo} className="see__all">
            SEE ALL
          </Link>
          <IconButton
            onClick={handlePrevSlider}
            disabled={arrowDisabled === "prev"}
            className="sliderArrow__icon"
          >
            <ChevronLeft />
          </IconButton>
          <IconButton
            onClick={handleNextSlider}
            disabled={arrowDisabled === "next"}
            className="sliderArrow__icon"
          >
            <ChevronRight />
          </IconButton>
        </div>
      </div>
      <div className="slider__body">
        <Slider
          slidesToShow={5}
          slidesToScroll={3}
          beforeChange={onBeforeChangeSlide}
          ref={sliderRef}
          className="main__slider"
        >
          {children}
        </Slider>
      </div>
    </div>
  );
}

export default MovieSlider;
