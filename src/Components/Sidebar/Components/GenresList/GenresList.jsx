import React from "react";
import { List, makeStyles, ListSubheader } from "@material-ui/core";
import { RadioButtonUnchecked, RadioButtonChecked } from "@material-ui/icons";

import ListItem from "../ListItem";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: "var(--bg-secondary)",
    justifyContent: "center",
  },
  subheader: {
    fontFamily: "GoldMan-Bold",
    fontSize: "1.2rem",
    fontWeight: "bold",
    position: "relative",
    color: "var(--txt-primary)",
  },
  uncheckedBtn: {
    display: "flex",
  },
  checkedBtn: {
    display: "none",
  },
  genresItem: {
    "&.list--active .unchecked": {
      display: "none",
    },
    "&.list--active .checked": {
      display: "flex",
    },
  },
}));

export default function GenresList({ list }) {
  const classes = useStyles();

  return (
    <div className={`${classes.root}`}>
      <List
        component="nav"
        aria-label="main mailbox folders"
        subheader={
          <ListSubheader
            component="div"
            id="nested-list-subheader"
            className={classes.subheader}
          >
            Genres
          </ListSubheader>
        }
      >
        {list.map((genre, index) => (
          <ListItem
            key={genre.id}
            to={{
              pathname: `/genres/${genre.name}`,
              state: {
                genreId: genre.id,
              },
            }}
            icon={
              <>
                <RadioButtonUnchecked
                  className={`${classes.uncheckedBtn} unchecked`}
                />
                <RadioButtonChecked
                  className={`${classes.checkedBtn} checked`}
                />
              </>
            }
            title={genre.name}
            className={classes.genresItem}
          />
        ))}
      </List>
    </div>
  );
}
