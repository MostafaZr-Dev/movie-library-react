import {
  ListItem as ListItemMui,
  ListItemIcon,
  ListItemText,
  makeStyles,
} from "@material-ui/core";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: "center",
    color: "var(--txt-inactive)",
  },
  active: {
    "&.list--active": {
      color: "var(--txt-active)",
    },
    "&.list--active .list__icon": {
      color: "var(--txt-active)",
    },
    "&.list--active .list__item": {
      borderRight: "4px solid var(--txt-active)",
    },
  },
}));

function ListItem({ to, icon, title, className }) {
  const classes = useStyles();

  return (
    <NavLink
      exact={true}
      to={to}
      activeClassName="list--active"
      className={`${classes.active} ${className}`}
    >
      <ListItemMui button className="list__item">
        <ListItemIcon className={`${classes.root} list__icon`}>
          {icon}
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItemMui>
    </NavLink>
  );
}

export default ListItem;
