import React from "react";
import { List, makeStyles } from "@material-ui/core";
import { Home, Favorite, Poll, EventNote } from "@material-ui/icons";

import ListItem from "../ListItem";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: "var(--bg-secondary)",
    justifyContent: "center",
  },
}));

export default function PrimaryList() {
  const classes = useStyles();

  return (
    <div className={`${classes.root}`}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem to="/" icon={<Home />} title="Home" />
        <ListItem to="/popular" icon={<Favorite />} title="Popular" />
        <ListItem to="/toprated" icon={<Poll />} title="Top Rated" />
        <ListItem to="/upcoming" icon={<EventNote />} title="Upcoming" />
      </List>
    </div>
  );
}
