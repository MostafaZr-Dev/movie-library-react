import "./Sidebar.css";

import PrimaryList from "./Components/PrimaryList";
import GenresList from "./Components/GenresList";
import { useGetGenres } from "Hooks";
import Scrollbar from "Components/Scrollbar";
import { useAppState } from "Store/useAppState";


function Sidebar() {
  const { genres, error, isLoading } = useGetGenres();
  const { state } = useAppState();

  return (
    <div className={`sidebar ${state.isMenuOpen ? "sidebar--open" : ""}`}>
      <Scrollbar>
        <div className="sidebar__wrapper">
          <div className="sidebar__logo">
            <img src={`${process.env.PUBLIC_URL}/logo.png`} alt="logo" />
          </div>
          <h1>Movie Library</h1>
          <PrimaryList />
          <GenresList list={genres} />
        </div>
      </Scrollbar>
    </div>
  );
}

export default Sidebar;
