import "./MenuToggle.css";
import { IconButton } from "@material-ui/core";
import { Close, Menu } from "@material-ui/icons";

function MenuToggle({ isOpen, onClick }) {
  return (
    <div className={`menu__toggle ${isOpen ? "menu__toggle--open" : ""}`}>
      <IconButton onClick={onClick} className="toggle__btn">
        {!isOpen && <Menu />}
        {isOpen && <Close />}
      </IconButton>
    </div>
  );
}

export default MenuToggle;
