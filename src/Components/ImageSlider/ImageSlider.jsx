import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import "react-awesome-slider/dist/styles.css";
import "./ImageSlider.css";

const AutoplaySlider = withAutoplay(AwesomeSlider);

function ImageSlider({ images }) {
  return (
    <AutoplaySlider
      play={true}
      cancelOnInteraction={false}
      interval={4000}
      organicArrows={false}
      buttons={false}
      bullets={true}
      className="image__slider"
    >
      {images}
    </AutoplaySlider>
  );
}

export default ImageSlider;
