import React, { forwardRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SimpleSlider = forwardRef(
  ({ children, slidesToShow, slidesToScroll, beforeChange, ...rest }, ref) => {
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: slidesToShow,
      slidesToScroll: slidesToScroll,
      vertical: false,
      verticalSwiping: false,
      swipeToSlide: true,
      arrows: false,
      beforeChange: beforeChange,
      lazyload: true,
      responsive: [
        {
          breakpoint: 1290,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 1180,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 1060,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3,
          },
        },

        {
          breakpoint: 940,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
        {
          breakpoint: 810,
          settings: {
            slidesToShow: 2.5,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 690,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 560,
          settings: {
            slidesToShow: 1.5,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <Slider ref={ref} {...settings} {...rest}>
        {children}
      </Slider>
    );
  }
);

export default SimpleSlider;
