import { Link } from "react-router-dom";
import { Skeleton } from "@material-ui/lab";
import "./MovieItem.css";

import { MotionHover } from "Components/Motion";
import LazyLoad from "Components/LazyLoad";
import Image from "Components/Image";

function MovieItem({
  to,
  primary,
  secondary,
  icon,
  imageSrc,
  itemClass,
  imgClass,
  isLazy,
  lazyScrollContainer,
}) {
  return (
    <div className={`movie__item ${itemClass ? itemClass : ""}`}>
      <Link to={to}>
        <MotionHover whileHover={{ scale: 1.1, boxShadow: "none" }}>
          <div className={`item__img ${imgClass ? imgClass : ""}`}>
            {!isLazy && <Image src={imageSrc} alt="movie-image" />}
            {isLazy && (
              <LazyLoad
                placeholder={
                  <Skeleton variant="rect" width={200} height={300} />
                }
                scrollContainer={lazyScrollContainer}
                overflow={true}
                scroll={false}
                debounce={500}
              >
                <Image src={imageSrc} alt="movie-image" />
              </LazyLoad>
            )}
          </div>
        </MotionHover>
      </Link>
      <div className="item__details">
        <span className="txt-primary">{primary}</span>
        <span className="txt-secondary">
          {icon}
          {secondary}
        </span>
      </div>
    </div>
  );
}

export default MovieItem;
