import "./Search.css";
import { useCallback, useState, useRef } from "react";
import { ClickAwayListener, IconButton, TextField } from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

import { MotionVariant } from "Components/Motion";

const searchVariants = {
  open: {},
  closed: {},
};

const txtFieldVariants = {
  open: {
    opacity: 1,
    width: "auto",
  },
  closed: {
    opacity: 0,
    width: 0,
  },
};

const Search = ({ className }) => {
  const [isOpen, setIsOpen] = useState(false);

  const history = useHistory();
  const searchFieldRef = useRef();

  const onClickAway = useCallback(() => {
    setIsOpen(false);
  }, []);

  const onIconClick = useCallback(() => {
    if (!isOpen) {
      return setIsOpen((prevState) => !prevState);
    }

    const searchValue = searchFieldRef.current.value;

    if (!searchValue || searchValue === "") {
      return;
    }

    if (searchValue.trim() === "") {
      searchFieldRef.current.value = "";
      return;
    }

    history.push({
      pathname: "/search",
      search: `?s=${searchValue}`,
    });
  }, [isOpen]);

  const onKeyUp = useCallback((e) => {
    if (e.keyCode !== 13) {
      return;
    }

    const searchValue = searchFieldRef.current.value;

    if (!searchValue || searchValue === "") {
      return;
    }

    if (searchValue.trim() === "") {
      searchFieldRef.current.value = "";
      return;
    }

    history.push({
      pathname: "/search",
      search: `?s=${searchValue}`,
    });
  }, []);

  return (
    <ClickAwayListener onClickAway={onClickAway}>
      <div className={className}>
        <MotionVariant
          tag="div"
          variants={searchVariants}
          animate={isOpen ? "open" : "closed"}
          initial="closed"
          className="search__wrapper"
        >
          <MotionVariant tag="div" variants={txtFieldVariants}>
            <TextField
              id="standard-secondary"
              placeholder="search for a movie..."
              onKeyUp={onKeyUp}
              inputRef={searchFieldRef}
              className="input__search"
            />
          </MotionVariant>
          <IconButton className="icon__search" onClick={onIconClick}>
            <SearchIcon />
          </IconButton>
        </MotionVariant>
      </div>
    </ClickAwayListener>
  );
};

export default Search;
