import { motion } from "framer-motion";

export const MotionHover = ({ children, whileHover, whileTap, ...props }) => (
  <motion.div whileHover={whileHover} whileTap={whileTap} {...props}>
    {children}
  </motion.div>
);
