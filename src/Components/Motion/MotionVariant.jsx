import { motion } from "framer-motion";

export const MotionVariant = ({ tag, children, variants, ...props }) => {
  const Component = motion[tag];

  return (
    <Component variants={variants} {...props}>
      {children}
    </Component>
  );
};
