import React from "react";
import { motion } from "framer-motion";

const MountTransition = ({
  children,
  slide,
  slideUp,
  exitSlide,
  exitSlideUp,
  duration = 0.5,
  ...props
}) => {
  return (
    <motion.div
      {...props}
      exit={{ opacity: 0, x: exitSlide, y: exitSlideUp }}
      initial={{ opacity: 0, x: slide, y: slideUp }}
      animate={{ opacity: 1, x: 0, y: 0 }}
      transition={{
        type: "tween",
        duration: duration,
      }}
      style={{ width: "100%", height: "100%" }}
    >
      {children}
    </motion.div>
  );
};

export default MountTransition;
