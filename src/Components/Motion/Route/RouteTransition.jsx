import React from "react";
import { Route } from "react-router-dom";

import MountTransition from "./MountTransition";

const RouteTransition = ({
  children,
  path,
  exact = false,
  slide = 0,
  slideUp = 0,
  exitSlide = 0,
  exitSlideUp = 0,
  ...rest
}) => {
  
  return (
    <Route exact={exact} path={path} {...rest}>
      <MountTransition
        slide={slide}
        slideUp={slideUp}
        exitSlide={exitSlide}
        exitSlideUp={exitSlideUp}
      >
        {children}
      </MountTransition>
    </Route>
  );
};

export default RouteTransition;
