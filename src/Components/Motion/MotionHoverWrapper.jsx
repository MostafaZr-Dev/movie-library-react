import { motion } from "framer-motion";

export const MotionHoverWrapper = ({
  children,
  initial,
  animate,
  whileHover,
}) => {
  return (
    <motion.div initial={initial} animate={animate} whileHover={whileHover}>
      {children}
    </motion.div>
  );
};
