import "./Rating.css";
import { Rating as RatinMui } from "@material-ui/lab";

function Rating({ value, ...props }) {
  return (
    <RatinMui
      className="rating"
      name="read-only"
      value={value}
      readOnly
      precision={0.5}
      {...props}
    />
  );
}

export default Rating;
