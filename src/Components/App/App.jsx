import { useEffect, useState } from "react";
import "./App.css";

import { AppLoader } from "Components/Loader";
import Sidebar from "Components/Sidebar";
import Body from "Components/Body";
import { useGetConfigs } from "Hooks";

function App() {
  const [isAppLoading, setIsAppLoading] = useState(true);


  useEffect(() => {
    document.onreadystatechange = function () {
      if (document.readyState === "complete") {
        setIsAppLoading(false);
      }
    };
  }, []);

  useGetConfigs();

  if (isAppLoading) {
    return <AppLoader />;
  }

  return (
    <div className="app">
      <Sidebar />
      <Body />
    </div>
  );
}

export default App;
