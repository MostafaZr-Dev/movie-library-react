import "./MovieWrapper.css";

import Scrollbar from "Components/Scrollbar";
import Search from "Components/Search";
import ToggleTheme from "Components/ToggleTheme";

function MovieWrapper({
  headerTitle,
  headerSubTitle,
  children,
  header,
  footer,
}) {
  return (
    <Scrollbar className="scroll__wrapper">
      <div className="movie__wrapper">
        <div className="wrapper__header">
          <div className="header__left">
            <h3 className="header__title">{headerTitle}</h3>
            <h6 className="header__subTitle">{headerSubTitle}</h6>
          </div>
          <div className="header__right">
            <ToggleTheme />
            <Search />
          </div>
        </div>
        {header}
        <div className="wrapper__body">{children}</div>
        {footer}
      </div>
    </Scrollbar>
  );
}

export default MovieWrapper;
