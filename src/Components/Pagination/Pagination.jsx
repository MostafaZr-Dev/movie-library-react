import { Chip } from "@material-ui/core";
import { ChevronLeft, ChevronRight } from "@material-ui/icons";
import "./Pagination.css";

function Pagination({ page, onPrevClick, onNextClick, className }) {
  return (
    <div className={`pagination ${className ? className : null}`}>
      <div className="prev">
        {page > 1 && (
          <Chip
            avatar={<ChevronLeft />}
            label={`Page ${page - 1}`}
            clickable
            color="primary"
            onClick={onPrevClick}
            className="btn"
          />
        )}
      </div>
      <div className="next">
        <Chip
          label={`Page ${page + 1}`}
          clickable
          color="primary"
          onDelete={onNextClick}
          deleteIcon={<ChevronRight />}
          onClick={onNextClick}
          className="btn"
        />
      </div>
    </div>
  );
}

export default Pagination;
