import { useCallback, useState } from "react";

function Image({ src, alt }) {
  const [isImageError, setIsImageError] = useState(false);

  const handleImageError = useCallback((e) => {
    setIsImageError(true);
  }, []);

  return (
    <>
      {!isImageError && <img src={src} alt={alt} onError={handleImageError} />}
      {isImageError && (
        <img
          src={`${process.env.PUBLIC_URL}/assets/img/noImage.png`}
          alt="movie-item-img"
          onError={handleImageError}
        />
      )}
    </>
  );
}

export default Image;
